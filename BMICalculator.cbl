       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMICALCULATOR.
       AUTHOR. CHONCHANOK.
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01 P-HEIGHT               PIC 999V9 VALUE ZERO.
       01 P-WEIGHT               PIC 999V9 VALUE ZERO.
       01 P-HEIGHT-M             PIC 999V9 VALUE ZERO.
       01 BMI                    PIC 999V9.

          88 UNDERWEIGHT                   VALUE 0.0 THRU 18.4.
          88 NORMALWEIGHT                  VALUE 18.5 THRU 24.9.
          88 OVERWEIGHT                    VALUE 25.0 THRU 29.9.
          88 OBESITY                       VALUE 30.0 THRU 39.9.
          88 EXTREMELYOBESITY              VALUE 40.0 THRU 99.0.

       01 BMI-MESSAGE            PIC X(25) VALUE SPACE.
          88 M-UNDERWEIGHT                 VALUE "Underweight".
          88 M-NORMALWEIGHT                VALUE "Normal weight".
          88 M-OVERWEIGHT                  VALUE "Overweight".
          88 M-OBESITY                     VALUE "Obesity ".
          88 M-EXTREMELYOBESITY            VALUE "Extremely Obesity".
       PROCEDURE DIVISION.
       BEGIN.          
           DISPLAY "Enter weight (km) - " WITH NO ADVANCING
           ACCEPT P-WEIGHT
           DISPLAY "Enter height (cm) - " WITH NO ADVANCING
           ACCEPT P-HEIGHT
           DIVIDE P-HEIGHT BY 100 GIVING P-HEIGHT-M 
           COMPUTE BMI = P-WEIGHT /(P-HEIGHT-M * P-HEIGHT-M)
           DISPLAY "BMI : " BMI
           EVALUATE TRUE 
           WHEN UNDERWEIGHT
                SET M-UNDERWEIGHT TO TRUE
           WHEN NORMALWEIGHT
                SET M-NORMALWEIGHT TO TRUE
           WHEN OVERWEIGHT
                SET M-OVERWEIGHT TO TRUE
           WHEN OBESITY
                SET M-OBESITY TO TRUE
           WHEN EXTREMELYOBESITY
                SET M-EXTREMELYOBESITY TO TRUE
           END-EVALUATE
           DISPLAY BMI-MESSAGE
           GOBACK
           .